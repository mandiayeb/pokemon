import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import core.Pokemon;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

public class Main {
    static OkHttpClient client = new OkHttpClient();

    public static void main(String[] args) throws IOException {
        String json = run("https://pokeapi.co/api/v2/pokemon/ditto");

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        Pokemon pokemon = objectMapper.readValue(json, Pokemon.class);
        System.out.println(pokemon.toString());
        System.out.println(pokemon.getForms().size());
    }

    public static String run(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
