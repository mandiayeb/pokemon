package core;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

public class Pokemon {
    ArrayList< Object > abilities = new ArrayList < Object > ();
    @JsonProperty("base_experience")
    private int baseExperience;
    ArrayList < Object > forms = new ArrayList < Object > ();
    @JsonProperty("game_indices")
    ArrayList < Object > gameIndices = new ArrayList < Object > ();
    private float height;
    @JsonProperty("held_items")
    ArrayList < Object > heldItems = new ArrayList < Object > ();
    private float id;
    @JsonProperty("is_default")
    private boolean isDefault;
    @JsonProperty("location_area_encounters")
    private String locationAreaEncounters;
    ArrayList < Object > moves = new ArrayList < Object > ();
    private String name;
    private float order;
    @JsonProperty("past_types")
    ArrayList < Object > pastTypes = new ArrayList < Object > ();
    Species SpeciesObject;
    Sprites SpritesObject;
    ArrayList < Object > stats = new ArrayList < Object > ();
    ArrayList < Object > types = new ArrayList < Object > ();
    private float weight;

    public ArrayList<Object> getAbilities() {
        return abilities;
    }

    public void setAbilities(ArrayList<Object> abilities) {
        this.abilities = abilities;
    }

    public float getBaseExperience() {
        return baseExperience;
    }

    public void setBaseExperience(int baseExperience) {
        this.baseExperience = baseExperience;
    }

    public ArrayList<Object> getForms() {
        return forms;
    }

    public void setForms(ArrayList<Object> forms) {
        this.forms = forms;
    }

    public ArrayList<Object> getGameIndices() {
        return gameIndices;
    }

    public void setGameIndices(ArrayList<Object> gameIndices) {
        this.gameIndices = gameIndices;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public ArrayList<Object> getHeldItems() {
        return heldItems;
    }

    public void setHeldItems(ArrayList<Object> heldItems) {
        this.heldItems = heldItems;
    }

    public float getId() {
        return id;
    }

    public void setId(float id) {
        this.id = id;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getLocationAreaEncounters() {
        return locationAreaEncounters;
    }

    public void setLocationAreaEncounters(String locationAreaEncounters) {
        this.locationAreaEncounters = locationAreaEncounters;
    }

    public ArrayList<Object> getMoves() {
        return moves;
    }

    public void setMoves(ArrayList<Object> moves) {
        this.moves = moves;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getOrder() {
        return order;
    }

    public void setOrder(float order) {
        this.order = order;
    }

    public ArrayList<Object> getPastTypes() {
        return pastTypes;
    }

    public void setPastTypes(ArrayList<Object> pastTypes) {
        this.pastTypes = pastTypes;
    }

    public Species getSpeciesObject() {
        return SpeciesObject;
    }

    public void setSpeciesObject(Species speciesObject) {
        SpeciesObject = speciesObject;
    }

    public Sprites getSpritesObject() {
        return SpritesObject;
    }

    public void setSpritesObject(Sprites spritesObject) {
        SpritesObject = spritesObject;
    }

    public ArrayList<Object> getStats() {
        return stats;
    }

    public void setStats(ArrayList<Object> stats) {
        this.stats = stats;
    }

    public ArrayList<Object> getTypes() {
        return types;
    }

    public void setTypes(ArrayList<Object> types) {
        this.types = types;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Pokemon{" +
                "abilities=" + abilities +
                ", baseExperience=" + baseExperience +
                ", forms=" + forms +
                ", gameIndices=" + gameIndices +
                ", height=" + height +
                ", heldItems=" + heldItems +
                ", id=" + id +
                ", isDefault=" + isDefault +
                ", locationAreaEncounters='" + locationAreaEncounters + '\'' +
                ", moves=" + moves +
                ", name='" + name + '\'' +
                ", order=" + order +
                ", pastTypes=" + pastTypes +
                ", SpeciesObject=" + SpeciesObject +
                ", SpritesObject=" + SpritesObject +
                ", stats=" + stats +
                ", types=" + types +
                ", weight=" + weight +
                '}';
    }
}
