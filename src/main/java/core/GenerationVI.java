package core;

public class GenerationVI {
    OmegarubyAlphasapphire OmegarubyAlphasapphireObject;
    Xy XyObject;


    // Getter Methods

    public OmegarubyAlphasapphire getOmegarubyAlphasapphire() {
        return OmegarubyAlphasapphireObject;
    }

    public Xy getXy() {
        return XyObject;
    }

    // Setter Methods

    public void setOmegarubyAlphasapphire(OmegarubyAlphasapphire omegarubyAlphasapphireObject) {
        this.OmegarubyAlphasapphireObject = omegarubyAlphasapphireObject;
    }

    public void setXy(Xy xyObject) {
        this.XyObject = xyObject;
    }
}
