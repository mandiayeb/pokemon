package core;

public class Versions {
    private GenerationI GenerationIObject;
    private GenerationII GenerationIIObject;
    private GenerationIII GenerationIIIObject;
    private GenerationIV GenerationIVObject;
    private GenerationV GenerationVObject;
    private GenerationVI GenerationVIObject;
    private GenerationVII GenerationVIIObject;
    private GenerationVIII GenerationVIIIObject;

    public GenerationI getGenerationIObject() {
        return GenerationIObject;
    }

    public void setGenerationIObject(GenerationI generationIObject) {
        GenerationIObject = generationIObject;
    }

    public GenerationII getGenerationIIObject() {
        return GenerationIIObject;
    }

    public void setGenerationIIObject(GenerationII generationIIObject) {
        GenerationIIObject = generationIIObject;
    }

    public GenerationIII getGenerationIIIObject() {
        return GenerationIIIObject;
    }

    public void setGenerationIIIObject(GenerationIII generationIIIObject) {
        GenerationIIIObject = generationIIIObject;
    }

    public GenerationIV getGenerationIVObject() {
        return GenerationIVObject;
    }

    public void setGenerationIVObject(GenerationIV generationIVObject) {
        GenerationIVObject = generationIVObject;
    }

    public GenerationV getGenerationVObject() {
        return GenerationVObject;
    }

    public void setGenerationVObject(GenerationV generationVObject) {
        GenerationVObject = generationVObject;
    }

    public GenerationVI getGenerationVIObject() {
        return GenerationVIObject;
    }

    public void setGenerationVIObject(GenerationVI generationVIObject) {
        GenerationVIObject = generationVIObject;
    }

    public GenerationVII getGenerationVIIObject() {
        return GenerationVIIObject;
    }

    public void setGenerationVIIObject(GenerationVII generationVIIObject) {
        GenerationVIIObject = generationVIIObject;
    }

    public GenerationVIII getGenerationVIIIObject() {
        return GenerationVIIIObject;
    }

    public void setGenerationVIIIObject(GenerationVIII generationVIIIObject) {
        GenerationVIIIObject = generationVIIIObject;
    }
}
