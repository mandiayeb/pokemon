package core;

public class GenerationIII {
    Emerald EmeraldObject;
    FireredLeafgreen FireredLeafgreenObject;
    RubySapphire RubySapphireObject;

    public Emerald getEmeraldObject() {
        return EmeraldObject;
    }

    public void setEmeraldObject(Emerald emeraldObject) {
        EmeraldObject = emeraldObject;
    }

    public FireredLeafgreen getFireredLeafgreenObject() {
        return FireredLeafgreenObject;
    }

    public void setFireredLeafgreenObject(FireredLeafgreen fireredLeafgreenObject) {
        FireredLeafgreenObject = fireredLeafgreenObject;
    }

    public RubySapphire getRubySapphireObject() {
        return RubySapphireObject;
    }

    public void setRubySapphireObject(RubySapphire rubySapphireObject) {
        RubySapphireObject = rubySapphireObject;
    }
}
