package core;

public class OfficialArtwork {
    private String front_default;

    // Getter Methods

    public String getFront_default() {
        return front_default;
    }

    // Setter Methods

    public void setFront_default(String front_default) {
        this.front_default = front_default;
    }
}
